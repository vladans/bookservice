﻿using BookService.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace BookService.Controllers
{
    public class AuthorsController : ApiController
    {
        private BookServiceContext db = new BookServiceContext();

        protected override void Dispose (bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET api/authors
        public IQueryable<Author> GetAuthors()
        {
            return db.Authors;
        }

        //GET api/authors/1
        [ResponseType(typeof(Author))]
        public IHttpActionResult GetAuthor(int id)
        {
            Author author = db.Authors.Find(id);
            if (author == null)
            {
                return NotFound();
            }
            return Ok(author);
        }

        // POST api/authors
        [ResponseType(typeof(Author))]
        public IHttpActionResult PostAuthor(Author author)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Authors.Add(author);
            db.SaveChanges();
            //return Ok(author);
            return CreatedAtRoute("DefaultApi", new { id = author.Id }, author);
        }

        // PUT api/authors/1
        [ResponseType(typeof(Author))]
        public IHttpActionResult PutAuthor(int id, Author author)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != author.Id)
            {
                return BadRequest();
            }

            db.Entry(author).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                bool authorExist = db.Authors.Count(x => x.Id == id) > 0;
                if (!authorExist)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(author);
        }

        // DELETE api/authors/1
        [ResponseType(typeof(Author))]
        public IHttpActionResult DeleteAuthor(int id)
        {
            Author author = db.Authors.Find(id);
            if (author == null)
            {
                return NotFound();
            }
            db.Authors.Remove(author);
            db.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
